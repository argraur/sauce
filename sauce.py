import aiohttp
import json

from .. import loader, utils, security
from io import BytesIO, StringIO
from requests import put
from telethon.utils import is_image

url = "https://saucenao.com/search.php?"
contentType = "application/vnd.api+json"
accept = contentType
db = "999"
outputType = "2"
testMode = "0"
numRes = "2"

@loader.tds
class SauceMod(loader.Module):
    strings = {'name': 'SauceNAO'}

    @loader.owner
    async def saucecmd(self, message):
        " - Найти источник изображения"
        reply = await message.get_reply_message()
        if not reply:
            await message.edit("<code>Reply to media!</code>")
            return
        media = reply.media
        if not media:
            await message.edit("<code>Media required</code>")
            return
        file = await message.client.download_file(media)
        await message.edit("<code>Downloaded successfully!</code>")
        try:
            response = put('https://transfer.sh/sauce.jpg', data=file)
        except ConnectionError:
            await message.edit("<code>Can't reach transfer.sh</code>")
            return
        path = response.text.replace(":", "%3A").replace("/", "%2F").replace("transfer.sh", "transfer.sh%2Fget")
        async with aiohttp.ClientSession() as session:
            async with session.post(f"{url}db={db}&output_type={outputType}&testmode={testMode}&numres={numRes}&url={path}") as raw_response:
                response = await raw_response.text()
                result = json.loads(response)
                print(result)
                

